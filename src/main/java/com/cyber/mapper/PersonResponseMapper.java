package com.cyber.mapper;

import org.dozer.DozerConverter;
import org.springframework.stereotype.Service;
import com.cyber.model.PersonProperty;
import com.cyber.web.response.PersonResponse;

@Service
public class PersonResponseMapper extends DozerConverter<PersonProperty, PersonResponse> {

    public PersonResponseMapper() {
        super(PersonProperty.class, PersonResponse.class);
    }

    @Override
    public PersonResponse convertTo(PersonProperty source, PersonResponse personResponse) {

        return PersonResponse.builder()
                .firstName(source.getPerson().getFirstName())
                .lastName(source.getPerson().getLastName())
                .age(source.getPerson().getAge())
                .baseSeed(source.getBaseSeed())
                .socialRatingScore(source.getSocialRatingScore())
                .build();
    }

    @Override
    public PersonProperty convertFrom(PersonResponse personResponse, PersonProperty source) {
        return null;
    }

}