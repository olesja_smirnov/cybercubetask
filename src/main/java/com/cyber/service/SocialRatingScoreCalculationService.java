package com.cyber.service;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cyber.config.ScoreConfig;
import com.cyber.model.PersonProperty;
import com.cyber.model.Person;
import com.cyber.repository.PersonPropertyRepository;
import com.cyber.repository.PersonRepository;
import com.cyber.web.request.PersonRequest;

@Service
@Slf4j
public class SocialRatingScoreCalculationService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonPropertyRepository personPropertyRepository;

    @Autowired
    private ScoreConfig scoreConfig;

    @Transactional
    public PersonProperty savePersonWithProperty(PersonRequest request) {
        Person newPerson = getNewPerson(request);
        Integer age = request.getAge();
        Double baseSeed = scoreConfig.getBaseSeed();

        Double socialRatingScore = age * baseSeed;

        PersonProperty personProperty = getPersonPropertyBuilder(baseSeed, socialRatingScore, newPerson);
        log.info("{} {} has {} score.", request.getFirstName(), request.getLastName(), personProperty.getSocialRatingScore());
        PersonProperty newPersonProp = personPropertyRepository.save(personProperty);

        return newPersonProp;
    }

    private Person getNewPerson(PersonRequest request) {
        Person person = Person.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .age(request.getAge())
                .build();

        return personRepository.save(person);
    }

    private PersonProperty getPersonPropertyBuilder(Double baseSeed, Double socialRatingScore, Person person) {

        return PersonProperty.builder()
                .baseSeed(baseSeed)
                .socialRatingScore(socialRatingScore)
                .person(person)
                .build();
    }

}