package com.cyber.repository;

import java.util.Optional;

import com.cyber.model.Person;
import com.cyber.model.PersonProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonPropertyRepository extends JpaRepository<PersonProperty, Long> {

    Optional<PersonProperty> findByPerson(Person person);

}