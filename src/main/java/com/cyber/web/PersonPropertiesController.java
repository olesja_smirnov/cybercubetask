package com.cyber.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.cyber.mapper.PersonResponseMapper;
import com.cyber.model.PersonProperty;
import com.cyber.service.SocialRatingScoreCalculationService;
import com.cyber.web.request.PersonRequest;
import com.cyber.web.response.PersonResponse;

@RestController
@RequestMapping(path = "/api/score")
@Api(tags = "score", value = "/api/score")
public class PersonPropertiesController {

    @Autowired
    private SocialRatingScoreCalculationService scoreService;

    @Autowired
    private PersonResponseMapper mapper;

    @ApiOperation("Save new person and his score")
    @PostMapping
    @ResponseBody
    public PersonResponse addPersonWithProperty(@RequestBody PersonRequest request) {
        PersonProperty response = scoreService.savePersonWithProperty(request);

        return mapper.convertTo(response);
    }

}