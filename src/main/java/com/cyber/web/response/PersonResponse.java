package com.cyber.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PersonResponse {

    private String firstName;

    private String lastName;

    private Integer age;

    private Double baseSeed;

    private Double socialRatingScore;

}