package com.cyber.config;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("score")
@Data
public class ScoreConfig {

    private Double baseSeed;

}