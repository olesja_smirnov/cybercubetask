# CyberCube Application
This project provides backend API

# Database
After starting the application pls navigate to http://localhost:8080/h2-console.

* DriverClassName: org.h2.Driver
* JDBC URL: jdbc:h2:file:~/test1
* User Name: username
* Password: password

# Swagger
You can send request via Swagger. Pls open below link.

http://localhost:8080/swagger-ui/


# Test task
Create a service written on Java and Spring Boot for calculating people&#39;s social rating.
The service should:
1. consume http requests with JSON in a body:
{
&quot;first_name&quot;: &lt;text&gt;,
&quot;last_name&quot;: &lt;text&gt;,
&quot;age&quot;: &lt;number&gt;
}
2. read calculation seed from application properties file (it can be just a random
number 0-1)
3. calculate social rating score (score = base seed * user’s age)
4. print similar to the following message to the application console
&quot;&lt;firstName&gt; &lt;lastName&gt; has &lt;socialRatingScore&gt; score&quot;
5. save result to database. It can be either a relational database (PostgreSQL,
MySQL) as well as NoSQL (for example Redis)
